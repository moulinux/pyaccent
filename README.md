# Remplacer les caractères accentués d'un texte

Voici un script sans prétention qui permet d'automatiser la création d'un
texte sans accent. C'est une activité intéressante que les élèves peuvent
faire en école primaire afin de s'exercer aux difficultés de la langue
française.

## Exécution dans un environnement python

Il suffit juste de lancer le programme : `python3 accent.py`

> Le script teste l'existence du fichier `mon_texte.txt`. Si celui-ci n'est
pas présent, l'invite de commandes permet de saisir le nom du fichier
qui est censé contenir le texte à traiter...

## Intégration continue

L'application profite de l'intégration continue offerte par Gitlab-ce afin
d'exécuter automatiquement le script dans un conteneur à chaque commit.
Une fois le projet cloné, il suffit juste, par exemple, de modifier le contenu
du fichier `mon_texte.txt`, d'enregistrer un commit pour que le fichier
transformé soit accessible dans les [pages][framapages] de Gitlab...

> Le texte de démonstration s'appuie sur un extrait de l'[article] qui a inspiré cette activité
et donc cette application : "Une avocate a réécrit les conditions d'utilisation d'Instagram
comme si elles étaient expliquées à un enfant de 8 ans".

[framapages]: https://moulinux.frama.io/pyaccent/mon_texte_sans_accent.txt
[article]: https://www.businessinsider.fr/une-avocate-a-reecrit-les-conditions-dutilisations-dinstagram-comme-si-elles-etaient-expliquees-a-un-enfant-de-8-ans/
