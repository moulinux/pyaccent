"""
    accent
    ~~~~~~

    Le programme remplace les caractères accentués d'un texte par des
    caractères ASCII adéquats.

    :copyright: (c) 2019 par moulinux.
    :license: AGPLv3, voir la LICENSE pour plus d'informations.
"""
from pathlib import Path


def lecture(un_nom, un_dico):
    """
    Lit un fichier texte et remplace les caractères accentués

    un_nom : nom du fichier
    un_dico : dictionnaire des caractères accentués

    Valeur de retour : chaîne de caractères
    """
    ch = ""
    with open(un_nom, "r", encoding="utf-8") as fic:
        c = True
        while c:
            c = fic.read(1)
            if c in dico:
                ch += dico[c]
            else:
                ch += c
        fic.close()
    return ch


def ecriture(une_chaine):
    """
    Ecrit une chaîne de caractères dans un fichier défini.

    Pas de valeur de retour
    """
    with open('mon_texte_sans_accent.txt', 'w') as fic:
        fic.write(une_chaine)
        fic.close()


# programme principal
dico = {
    'é': 'e',
    'è': 'e',
    'à': 'a',
    'û': 'u',
    'ô': 'o',
    'î': 'i',
    'À': 'A',
}

nom_fichier = "mon_texte.txt"
mon_fichier = Path(nom_fichier)
if not(mon_fichier.is_file()):
    nom_fichier = input("Entrer le nom du fichier : ")
la_chaine = lecture(nom_fichier, dico)
if la_chaine:
    ecriture(la_chaine)
